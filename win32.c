/*
 *          Copyright 2021, Vitali Baumtrok.
 * Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE or copy at
 *        http://www.boost.org/LICENSE_1_0.txt)
 */

#if defined(OGLWND_WIN32)

#include "oglwnd.h"
#include "win32.h"
#include "win32_init.h"

void oglwnd_window_destroy(void *const data) {
	if (data) {
		wnd_data_t *const wnd_data = (wnd_data_t*)data;
		window_release(&wnd_data->window);
		/* stop event queue thread */
		if (!is_class_registered(wnd_data->window.cls.lpszClassName))
			PostQuitMessage(0);
		free(wnd_data);
	}
}

static void update_client_props(wnd_data_t *const wnd_data, const int width, const int height) {
	POINT point = { 0, 0 };
	ClientToScreen(wnd_data->window.hndl, &point);
	wnd_data->client.x = point.x;
	wnd_data->client.y = point.y;
	wnd_data->client.width = width;
	wnd_data->client.height = height;
}

static void backup_client_props(wnd_data_t *const wnd_data) {
	wnd_data->client.x_wnd = wnd_data->client.x;
	wnd_data->client.y_wnd = wnd_data->client.y;
	wnd_data->client.width_wnd = wnd_data->client.width;
	wnd_data->client.height_wnd = wnd_data->client.height;
}

static void update_clip_cursor(wnd_data_t *const wnd_data) {
	client_t *const client = &(wnd_data->client);
	const RECT rect = { client->x, client->y, client->x + client->width, client->y + client->height };
	ClipCursor(&rect);
	wnd_data->state.locked = 1;
}

static void set_fullscreen(wnd_data_t *const wnd_data) {
	backup_client_props(wnd_data);
	SetWindowLong(wnd_data->window.hndl, GWL_STYLE, 0);
	SetWindowPos(wnd_data->window.hndl, HWND_TOP, monitor.x, monitor.y, monitor.width, monitor.height, SWP_NOOWNERZORDER | SWP_FRAMECHANGED | SWP_SHOWWINDOW);
	if (wnd_data->config.locked)
		update_clip_cursor(wnd_data);
}

/*
static DWORD get_style() {
	DWORD style;
	if (wnd_data->config.borderless)
		if (wnd_data->config.resizable)
			style = WS_POPUP;
		else
			style = WS_POPUP;
	else
		if (wnd_data->config.resizable)
			style = WS_OVERLAPPEDWINDOW;
		else
			style = WS_OVERLAPPEDWINDOW & ~WS_THICKFRAME & ~WS_MAXIMIZEBOX;
	return style;
}
*/

void oglwnd_free_mem(void *const mem) {
	free(mem);
}

void oglwnd_process_events() {
	MSG msg;
	while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}

void oglwnd_process_events_blocking() {
	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0) > 0) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}

void oglwnd_init(int *const err, oglwnd_ul_t *const err_win32) {
	int error = 0;
	oglwnd_ul_t error_win32 = 0;
	module_init(&error, &error_win32);
	dummy_class_init(&error, &error_win32);
	dummy_window_create(&error, &error_win32);
	dummy_context_init(&error, &error_win32);
	wgl_functions_init(&error, &error_win32);
	monitor_init(&error, &error_win32);
	window_release(&dummy);
	err[0] = error;
	err_win32[0] = error_win32;
}

void oglwnd_window_new(void **const data, void *const go_obj, const int x, const int y, const int w, const int h, const int wn, const int hn,
	const int wx, const int hx, const int b, const int d, const int r, const int f, const int l, const int c, int *const err, oglwnd_ul_t *const err_win32, char **const err_str) {
	if (err[0] == 0) {
		int error = 0;
		oglwnd_ul_t error_win32 = 0;
		char *error_str = NULL;
		wnd_data_t **const wnd_data = (wnd_data_t**)data;
		window_alloc(wnd_data, go_obj, &error, &error_win32, &error_str);
		if (error == 0) {
			config_t *const config = &wnd_data[0]->config;
			config->x = x;
			config->y = y;
			config->width = w;
			config->height = h;
			config->width_min = wn;
			config->height_min = hn;
			config->width_max = wx;
			config->height_max = hx;
			config->centered = c;
			config->borderless = b;
			config->dragable = d;
			config->resizable = r;
			config->fullscreen = f;
			config->locked = l;
			config_ensure(config);
		} else {
			oglwnd_window_destroy(data[0]);
			err[0] = error;
			err_win32[0] = error_win32;
			err_str[0] = error_str;
		}
	}
}

void oglwnd_window_init(void *const data, int *const err, oglwnd_ul_t *const err_win32, char **const err_str) {
	if (err[0] == 0) {
		int error = 0;
		oglwnd_ul_t error_win32 = 0;
		char *error_str = NULL;
		wnd_data_t *const wnd_data = (wnd_data_t*)data;
		window_class_init(wnd_data, &error, &error_win32, &error_str);
		window_create(wnd_data, &error, &error_win32, &error_str);
		window_context_init(wnd_data, &error, &error_win32, &error_str);
		if (error) {
			oglwnd_window_destroy(data);
			err[0] = error;
			err_win32[0] = error_win32;
			err_str[0] = error_str;
		}
	}
}

void oglwnd_show(void *const data) {
	if (data) {
		RECT rect;
		wnd_data_t *const wnd_data = (wnd_data_t*)data;
		ShowWindow(wnd_data->window.hndl, SW_SHOWDEFAULT);
		GetClientRect(wnd_data->window.hndl, &rect);
		update_client_props(wnd_data, (int)(rect.right - rect.left), (int)(rect.bottom - rect.top));
		if (wnd_data->config.fullscreen)
			set_fullscreen(wnd_data);
		GetClientRect(wnd_data->window.hndl, &rect);
		update_client_props(wnd_data, (int)(rect.right - rect.left), (int)(rect.bottom - rect.top));
		goOnShow(wnd_data->go_obj);
	}
}

void oglwnd_context(void *const data, void **dc, void **rc) {
	if (data) {
		wnd_data_t *const wnd_data = (wnd_data_t*)data;
		dc[0] = (void*)wnd_data->window.dc;
		rc[0] = (void*)wnd_data->window.rc;
	}
}

void oglwnd_ctx_make_current(void *const dc, void *const rc, int *const err, oglwnd_ul_t *const err_win32, char **const err_str) {
	if (!wglMakeCurrent((HDC)dc, (HGLRC)rc)) {
		err[0] = 17;
		err_win32[0] = GetLastError();
	}
}

void oglwnd_ctx_release(int *const err, oglwnd_ul_t *const err_win32, char **const err_str) {
	if (!wglMakeCurrent(NULL, NULL)) {
		err[0] = 18;
		err_win32[0] = GetLastError();
	}
}

void oglwnd_swap_buffers(void *const dc, int *const err, oglwnd_ul_t *const err_win32, char **const err_str) {
	if (!SwapBuffers((HDC)dc)) {
		err[0] = 19;
		err_win32[0] = GetLastError();
	}
}

void oglwnd_get_window_props(void *const data, int *x, int *y, int *w, int *h, int *wn, int *hn, int *wx, int *hx, int *b, int *d, int *r, int *f, int *l) {
	if (data) {
		wnd_data_t *const wnd_data = (wnd_data_t*)data;
		x[0] = wnd_data->client.x;
		y[0] = wnd_data->client.y;
		w[0] = wnd_data->client.width;
		h[0] = wnd_data->client.height;
		wn[0] = wnd_data->config.width_min;
		hn[0] = wnd_data->config.height_min;
		wx[0] = wnd_data->config.width_max;
		hx[0] = wnd_data->config.height_max;
		b[0] = wnd_data->config.borderless;
		d[0] = wnd_data->config.dragable;
		r[0] = wnd_data->config.resizable;
		f[0] = wnd_data->config.fullscreen;
		l[0] = wnd_data->config.locked;
	}
}

void oglwnd_set_window_props(void *const data, int x, int y, int w, int h, int wMin, int hMin, int wMax, int hMax, int b, int d, int r, int f, int l) {
	if (data) {
		wnd_data_t *const wnd_data = (wnd_data_t*)data;
		const int xywh = (x != wnd_data->client.x || y != wnd_data->client.y || w != wnd_data->client.width || h != wnd_data->client.height);
		const int mm = (wMin != wnd_data->config.width_min || hMin != wnd_data->config.height_min || wMax != wnd_data->config.width_max || hMax != wnd_data->config.height_max);
		const int stl = (b != wnd_data->config.borderless || r != wnd_data->config.resizable);
		const int fs = (f != wnd_data->config.fullscreen);
		wnd_data->client.x = x;
		wnd_data->client.y = y;
		wnd_data->client.width = w;
		wnd_data->client.height = h;
		wnd_data->config.width_min = wMin;
		wnd_data->config.height_min = hMin;
		wnd_data->config.width_max = wMax;
		wnd_data->config.height_max = hMax;
		wnd_data->config.borderless = b;
		wnd_data->config.dragable = d;
		wnd_data->config.resizable = r;
		wnd_data->config.fullscreen = f;
		// fullscreen
		/*
		if (fs && f) {
			set_fullscreen();
		// window
		} else if (fs) {
			if (!xywh)
				restore_client_props();
			set_window_pos(client.x, client.y, client.width, client.height);
		} else if (!f) {
			if (stl) {
				set_window_pos(x, y, w, h);
			} else if (xywh) {
				move_window(x, y, w, h);
			}
		}
		set_mouse_locked(l);
		*/
	}
}

#include "win32_proc.h"

/* #if defined(_OGLWIN_WIN32) */
#endif
